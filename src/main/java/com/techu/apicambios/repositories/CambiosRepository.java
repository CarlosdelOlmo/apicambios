package com.techu.apicambios.repositories;


import com.techu.apicambios.models.CambiosModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CambiosRepository extends MongoRepository<CambiosModel, String> {
}
