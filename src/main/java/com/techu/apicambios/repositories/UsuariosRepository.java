package com.techu.apicambios.repositories;

import com.techu.apicambios.models.UsuariosModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by U501257 on 10/06/2021.
 */

@Repository
public interface UsuariosRepository extends MongoRepository<UsuariosModel, String>{
}

