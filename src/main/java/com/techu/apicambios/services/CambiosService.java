package com.techu.apicambios.services;

import com.techu.apicambios.models.CambiosModel;
import com.techu.apicambios.repositories.CambiosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CambiosService {

    @Autowired
    CambiosRepository cambiosRepository;

    public List<CambiosModel> findAll(){

        return this.cambiosRepository.findAll();
    }

    public CambiosModel add(CambiosModel cambio){
        System.out.println("add_cambio");

        return this.cambiosRepository.save(cambio);
    }

    public Optional<CambiosModel> findById(String id){
        System.out.println("findById_cambio");

        return this.cambiosRepository.findById(id);
    }

    public CambiosModel update(CambiosModel cambio){
        System.out.println("update_cambio");

        return this.cambiosRepository.save(cambio);
    }

    public boolean delete(String id){
        System.out.println("delete_cambio");

        boolean result = false;

        if (this.cambiosRepository.findById(id).isPresent() == true){
            System.out.println("Cambio ENCONTRADO, borrando...");
            this.cambiosRepository.deleteById(id);
            result = true;
        }
        else {
            System.out.println("Cambio NO encontrado");
        }

        return result;

    }

    public List<CambiosModel> getCambios(String orderBy){
        System.out.println("getCambios_services");
        //return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy));
        if(orderBy != null) {
            if (orderBy.equals("age")) {
                return this.cambiosRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
            }
        }

        return this.cambiosRepository.findAll();
    }

}
