package com.techu.apicambios.services;

import com.techu.apicambios.models.UsuariosModel;
import com.techu.apicambios.repositories.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by U501257 on 10/06/2021.
 */

@Service
public class UsuariosService {

    @Autowired
    UsuariosRepository usuariosRepository;

    public UsuariosModel addUsuario(UsuariosModel usuario) {
        System.out.println("addUsuario");

        return this.usuariosRepository.save(usuario);
    }

    public List<UsuariosModel> findAll(){
        System.out.println("getUsuarios");
        return this.usuariosRepository.findAll();
    }

    public Optional<UsuariosModel> findById(String id) {
        System.out.println("findById");
        System.out.println("La id del usuario a buscar es " + id);

        return this.usuariosRepository.findById(id);
    }

    public UsuariosModel update(UsuariosModel user) {
        System.out.println("updateUsuario");

        return this.usuariosRepository.save(user);
    }


    public boolean delete(String id) {
        System.out.println("deleteUsuarios");

        boolean result = false;

        if (this.usuariosRepository.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");
            this.usuariosRepository.deleteById(id);

            result = true;
        }


        return result;
    }


}

