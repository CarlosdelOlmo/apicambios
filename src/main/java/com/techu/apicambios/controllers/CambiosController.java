package com.techu.apicambios.controllers;

import com.techu.apicambios.models.CambiosModel;
import com.techu.apicambios.models.UsuariosModel;
import com.techu.apicambios.repositories.UsuariosRepository;
import com.techu.apicambios.services.CambiosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apicambios")
@CrossOrigin(origins = "*" , methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.POST,RequestMethod.PATCH})

public class CambiosController {

    @Autowired
    CambiosService cambiosService;
    @Autowired
    UsuariosRepository usuariosRepository;


    @GetMapping("/cambios")
    public ResponseEntity<List<CambiosModel>> getCambios() {
        System.out.println("getCambios");
        return new ResponseEntity<>(this.cambiosService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/cambio")
    public ResponseEntity<CambiosModel> addCambio(@RequestBody CambiosModel cambio) {
        System.out.println("addCambio");
        System.out.println("La id del cambio que se va a crear es " + cambio.getId());
        System.out.println("La descripcion del cmabio que se va a crear es " + cambio.getDesc());
        System.out.println("La fecha de alta del cambio que se va a crear es " + cambio.getFecAlta());
        System.out.println("El id del usuario peticionario de la nueva petición es:  " + cambio.getIdUserPet());
        System.out.println("El id del usuario autorizador de la nueva petición es:  " + cambio.getIdUserAut());
        System.out.println("La fecha de la autorizacion de la nueva petición es:  " + cambio.getFecAut());
        System.out.println("El id del usuario resolutor es:  " + cambio.getIdUserRes());
        System.out.println("La fecha de la resolución de la nueva petición es:  " + cambio.getFecRes());

        return new ResponseEntity<>(this.cambiosService.add(cambio), HttpStatus.CREATED);
    }

    @GetMapping("/cambios/{id}")
    public ResponseEntity<Object> getCambioById(@PathVariable String id) {
        System.out.println("getCambioById");
        System.out.println("La id del cambio a buscar es " + id);
        Optional<CambiosModel> result = this.cambiosService.findById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Cambio NO ENCONTRADO",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Devuelve las peticiones de un usuario rol Peticionario
    @GetMapping("/petbyuserpet/{idUserPet}")
    public ResponseEntity<List<CambiosModel>> getCambioByidUserPet(@PathVariable String idUserPet) {
        System.out.println("getCambioByidUserPet");
        System.out.println("La id del usuario peticionario es " + idUserPet);

        List<CambiosModel> list =  this.cambiosService.findAll();
        List<CambiosModel> result = new ArrayList<CambiosModel>();
        System.out.println("Tamaño de la lista es: " + list.size());

        for (int i=0; i<=list.size()-1; i++)
        {
            if (list.get(i).getIdUserPet().equals(idUserPet)){
                System.out.println("Iteración " + i + " Usuario " + list.get(i).getIdUserRes() + " usuario coincide con el usuario " + idUserPet);
                result.add(list.get(i));
            }
            else{
                System.out.println("Iteración " + i + " Usuario " + list.get(i).getIdUserRes() + " usuario NO coincide con el usuario " + idUserPet);
            }
        }
        System.out.println("Tamaño del result es: " + result.size());

        return new ResponseEntity<>(result, result.size()>0 ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    //Devuelve las peticiones de un usuario rol Autorizador
    @GetMapping("/petbyuseraut/{idUserAut}")
    public ResponseEntity<List<CambiosModel>> getCambioByidUserAut(@PathVariable String idUserAut) {
        System.out.println("getCambioByidUserPet");
        System.out.println("La id del usuario autorizador es " + idUserAut);

        List<CambiosModel> list =  this.cambiosService.findAll();
        List<CambiosModel> result = new ArrayList<CambiosModel>();
        System.out.println("Tamaño de la lista es: " + list.size());

        for (int i=0; i<=list.size()-1; i++)
        {
            if (list.get(i).getIdUserAut().equals(idUserAut)){
                System.out.println("Iteración " + i + " Usuario " + list.get(i).getIdUserRes() + " usuario coincide con el usuario " + idUserAut);
                result.add(list.get(i));
            }
            else{
                System.out.println("Iteración " + i + " Usuario " + list.get(i).getIdUserRes() + " usuario NO coincide con el usuario " + idUserAut);
            }
        }
        System.out.println("Tamaño del result es: " + result.size());

        return new ResponseEntity<>(result, result.size()>0 ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    //Devuelve las peticiones que no están autorizadas
    @GetMapping("/petuseraut")
    public ResponseEntity<List<CambiosModel>> getCambioSinAut() {
        System.out.println("getCambioSinAut");

        List<CambiosModel> list =  this.cambiosService.findAll();
        List<CambiosModel> result = new ArrayList<CambiosModel>();
        System.out.println("Tamaño de la lista es: " + list.size());

        for (int i=0; i<=list.size()-1; i++)
        {
            if (list.get(i).getIdUserAut().equals("")){
                System.out.println("Petición a añadir al listado " + list.get(i).getId() + "usuario Autorizador es: " + list.get(i).getIdUserAut());
                result.add(list.get(i));
            }
            else{
                System.out.println("Petición no añadida al listado " + list.get(i).getId() + "usuario Autorizador es: " + list.get(i).getIdUserAut());
            }
        }
        System.out.println("Tamaño del result es: " + result.size());

        return new ResponseEntity<>(result, result.size()>0 ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    //Devuelve las peticiones de un usuario rol Resolutor
    @GetMapping("/petbyuserres/{idUserRes}")
    public ResponseEntity<List<CambiosModel>> getCambioByidUserRes(@PathVariable String idUserRes) {
        System.out.println("getCambioByidUserRes");
        System.out.println("La id del usuario resolutor es " + idUserRes);

        List<CambiosModel> list =  this.cambiosService.findAll();
        List<CambiosModel> result = new ArrayList<CambiosModel>();
        System.out.println("Tamaño de la lista es: " + list.size());

        for (int i=0; i<=list.size()-1; i++)
        {
            if (list.get(i).getIdUserRes().equals(idUserRes)){
                System.out.println("Iteración " + i + " Usuario " + list.get(i).getIdUserRes() + " usuario coincide con el usuario " + idUserRes);
                result.add(list.get(i));
            }
            else{
                System.out.println("Iteración " + i + " Usuario " + list.get(i).getIdUserRes() + " usuario NO coincide con el usuario " + idUserRes);
            }
        }
        System.out.println("Tamaño del result es: " + result.size());

        return new ResponseEntity<>(result, result.size()>0 ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    //Devuelve las peticiones no Resueltas
    @GetMapping("/petuserres")
    public ResponseEntity<List<CambiosModel>> getCambiosSinRes() {
        System.out.println("getCambiosSinRes");

        List<CambiosModel> list =  this.cambiosService.findAll();
        List<CambiosModel> result = new ArrayList<CambiosModel>();
        System.out.println("Tamaño de la lista es: " + list.size());

        for (int i=0; i<=list.size()-1; i++)
        {
            if (list.get(i).getIdUserRes().equals("") && !list.get(i).getIdUserAut().equals("")){
                System.out.println("Petición a añadir al listado " + list.get(i).getId() + "usuario Resolutor es: " + list.get(i).getIdUserRes());
                result.add(list.get(i));
            }
            else{
                System.out.println("Petición no añadida al listado " + list.get(i).getId() + "usuario Resolutor es: " + list.get(i).getIdUserRes());
            }
        }
        System.out.println("Tamaño del result es: " + result.size());

        return new ResponseEntity<>(result, result.size()>0 ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    @PutMapping("/cambios/{id}")
    public ResponseEntity<CambiosModel> updateCambio(@RequestBody CambiosModel cambiosModel, @PathVariable String id) {
        System.out.println("updateCambio");
        System.out.println("La id del cambio que se va a actualizar es: " + cambiosModel.getId());
        System.out.println("La descripcion del camabio que se va a actualizar es: " + cambiosModel.getDesc());
        System.out.println("La fecha de alta del cambio que se va a actualizar es: " + cambiosModel.getFecAlta());
        System.out.println("El id del usuario peticionario que se va a actualizar es:  " + cambiosModel.getIdUserPet());
        System.out.println("El id del usuario autorizador  que se va a actualizar es:  " + cambiosModel.getIdUserAut());
        System.out.println("La fecha de la autorizacion que se va a actualizar es:  " + cambiosModel.getFecAut());
        System.out.println("El id del usuario resolutor  que se va a actualizar es:  " + cambiosModel.getIdUserRes());
        System.out.println("La fecha de la resolución  que se va a actualizar es:  " + cambiosModel.getFecRes());

      Optional<CambiosModel> cambioToUpdate = this.cambiosService.findById(id);
        if (cambioToUpdate.isPresent()) {
            System.out.println("Cambio para actualizar ENCONTRADO, actualizando");
            this.cambiosService.update(cambiosModel);
        }
        return new ResponseEntity<>(cambiosModel,
                cambioToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/cambios/{id}")
    public ResponseEntity<String> deleteCambio(@PathVariable String id) {
        System.out.println("deleteCambio");
        System.out.println("La id del cambio a borrar es " + id);
        boolean deletedCambio = this.cambiosService.delete(id);
        return new ResponseEntity<>(
                deletedCambio ? "Cambio borrado" : "Cambio no borrado",
                deletedCambio ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
