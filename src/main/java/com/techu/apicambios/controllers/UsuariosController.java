package com.techu.apicambios.controllers;

import com.techu.apicambios.models.UsuariosModel;
import com.techu.apicambios.services.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by U501257 on 10/06/2021.
 */

@RestController
@RequestMapping("/apicambios")

public class UsuariosController {
    @Autowired
    UsuariosService usuariosService;

    @PostMapping("/usuarios")
    @CrossOrigin(origins = "*", methods ={RequestMethod.POST})
    public ResponseEntity<UsuariosModel> addUser(@RequestBody UsuariosModel usuario) {
        System.out.println("addUsuario");
        System.out.println("La id del usuario que se va a crear es " + usuario.getUserId());
        System.out.println("El nombre del usuario que se va a crear es " + usuario.getName());
        System.out.println("El apellido 1 del usuario que se va a crear es " + usuario.getFirstname());
        System.out.println("El apellido 2 del usuario que se va a crear es " + usuario.getSurname());
        System.out.println("El rol del usuario que se va a crear es " + usuario.getProfile());

        return new ResponseEntity<>(this.usuariosService.addUsuario(usuario), HttpStatus.CREATED);
    }

    @GetMapping("/usuarios")
    @CrossOrigin(origins = "*", methods ={RequestMethod.GET})
    public ResponseEntity<List<UsuariosModel>> getUsuarios(){
        System.out.println("getUsuarios");

        return new ResponseEntity<>(this.usuariosService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/usuarios/{id}")
    @CrossOrigin(origins = "*", methods ={RequestMethod.GET})
    public ResponseEntity<Object> getUsuarioById(@PathVariable String id) {
        System.out.println("getUsuarioById");
        System.out.println("La id del usuario a buscar es " + id);


        Optional<UsuariosModel> result = this.usuariosService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/usuarios/{id}")
    @CrossOrigin(origins = "*", methods ={RequestMethod.PUT})
    public ResponseEntity<UsuariosModel> updateUser(@RequestBody UsuariosModel usuario, @PathVariable String id){
        System.out.println("updateUsuario");
        System.out.println("La id del usuario que se va a actualizar en parámetro URL es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + usuario.getUserId());
        System.out.println("El nombre del usuario que se va a actualizar es " + usuario.getName());
        System.out.println("El primer apellido del usuario que se va a actualizar es " + usuario.getFirstname());
        System.out.println("El segundo apellido del usuario que se va a actualizar es " + usuario.getSurname());
        System.out.println("El rol del usuario que se va a actualizar es " + usuario.getProfile());

        Optional<UsuariosModel> userToUpdate = this.usuariosService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.usuariosService.update(usuario);
        }

        return new ResponseEntity<>(usuario,
                userToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


    @DeleteMapping("/usuarios/{id}")
    @CrossOrigin(origins = "*", methods ={RequestMethod.DELETE})
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUsuario");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deletedUser = this.usuariosService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }



}
