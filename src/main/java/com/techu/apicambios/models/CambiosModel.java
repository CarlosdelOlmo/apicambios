package com.techu.apicambios.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "cambios")
public class CambiosModel {

    @Id
    private String id;
    private String desc;
    private String fecAlta;
    private String idUserPet;
    private String idUserAut;
    private String fecAut;
    private String idUserRes;
    private String fecRes;

    public CambiosModel() {
    }

    public CambiosModel(String id, String desc, String fecAlta, String idUserPet, String idUserAut, String fecAut, String idUserRes, String fecRes) {
        this.id = id;
        this.desc = desc;
        this.fecAlta = fecAlta;
        this.idUserPet = idUserPet;
        this.idUserAut = idUserAut;
        this.fecAut = fecAut;
        this.idUserRes = idUserRes;
        this.fecRes = fecRes;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFecAlta() {
        return this.fecAlta;
    }

    public void setFecAlta(String fecAlta) {
        this.fecAlta = fecAlta;
    }

    public String getIdUserPet() {
        return this.idUserPet;
    }

    public void setIdUserPet(String idUserPet) {
        this.idUserPet = idUserPet;
    }

    public String getIdUserAut() {
        return this.idUserAut;
    }

    public void setIdUserAut(String idUserAut) {
        this.idUserAut = idUserAut;
    }

    public String getFecAut() {
        return this.fecAut;
    }

    public void setFecAut(String fecAut) {
        this.fecAut = fecAut;
    }

    public String getIdUserRes() {
        return this.idUserRes;
    }

    public void setIdUserRes(String idUserRes) {
        this.idUserRes = idUserRes;
    }

    public String getFecRes() {
        return this.fecRes;
    }

    public void setFecRes(String fecRes) {
        this.fecRes = fecRes;
    }
}
