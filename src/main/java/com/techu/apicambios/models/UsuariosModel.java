package com.techu.apicambios.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by U501257 on 10/06/2021.
 */
@Document(collection = "usuarios")

public class UsuariosModel {
    @Id

    private String userId;
    private String name;
    private String firstname;
    private String surname;
    private String profile;

    public UsuariosModel() {
    }

    public UsuariosModel(String userId, String name, String firstname, String surname, String profile) {
        this.userId = userId;
        this.name = name;
        this.firstname = firstname;
        this.surname = surname;
        this.profile = profile;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}

