package com.techu.apicambios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApicambiosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApicambiosApplication.class, args);
	}

}
